import {
  Box,
  Button,
  Container,
  Flex,
  Grid,
  Heading,
  Icon,
  Image,
  Input,
  Link,
} from '@chakra-ui/react';
import React from 'react';
import NextLink from 'next/link';
import {
  FaFacebookSquare,
  FaInstagram,
  FaTwitter,
  FaYoutube,
} from 'react-icons/fa';

const Footer = () => {
  return (
    <Container maxW={{ md: '5xl' }} py={{ md: 16 }}>
      <Grid
        templateColumns={{ base: 'repeat(3, 1fr)', md: 'repeat(4, 1fr)' }}
        gap={{ base: 5, md: 'normal' }}
      >
        <Box>
          <Image
            h={{ base: 24, md: 40 }}
            w={{ base: 24, md: 40 }}
            src='/tigereye.jpg'
            alt='tiger eye foundation logo'
          />
          <Flex align='center' mt={2}>
            <Link
              _hover={{ textDecor: 'none' }}
              href='https://www.facebook.com/TigerEyeFoundation/'
              isExternal
            >
              <Icon as={FaFacebookSquare} boxSize={5} />
            </Link>
            <Link
              _hover={{ textDecor: 'none' }}
              pl={{ base: 1, md: 6 }}
              href='https://www.instagram.com/tigereyefoundation/'
              isExternal
            >
              <Icon as={FaInstagram} boxSize={5} />
            </Link>
            <Link
              _hover={{ textDecor: 'none' }}
              pl={{ base: 1, md: 6 }}
              href='https://twitter.com/tigereyefound'
              isExternal
            >
              <Icon as={FaTwitter} boxSize={5} />
            </Link>
            <Link
              _hover={{ textDecor: 'none' }}
              pl={{ base: 1, md: 6 }}
              href='#'
              isExternal
            >
              <Icon as={FaYoutube} boxSize={5} />
            </Link>
          </Flex>
        </Box>
        <Box>
          <Heading
            as='h5'
            fontSize={{ base: 'sm', md: 'md' }}
            mb={4}
            textTransform='uppercase'
          >
            Foundation
          </Heading>
          <Box as='ul' listStyleType='none'>
            <NextLink href='/stories' passHref>
              <Link
                _hover={{ textDecor: 'none' }}
                as='li'
                py={1}
                fontSize={{ base: 'xs', md: 'sm' }}
              >
                Stories
              </Link>
            </NextLink>
            <NextLink href='/watch' passHref>
              <Link
                _hover={{ textDecor: 'none' }}
                as='li'
                py={1}
                fontSize={{ base: 'xs', md: 'sm' }}
              >
                Watch
              </Link>
            </NextLink>
            <NextLink href='/read' passHref>
              <Link
                _hover={{ textDecor: 'none' }}
                as='li'
                py={1}
                fontSize={{ base: 'xs', md: 'sm' }}
              >
                Read
              </Link>
            </NextLink>
            <NextLink href='/listen' passHref>
              <Link
                _hover={{ textDecor: 'none' }}
                as='li'
                py={1}
                fontSize={{ base: 'xs', md: 'sm' }}
              >
                Listen
              </Link>
            </NextLink>
          </Box>
        </Box>
        <Box>
          <Heading
            as='h5'
            fontSize={{ base: 'sm', md: 'md' }}
            mb={4}
            textTransform='uppercase'
          >
            Information
          </Heading>
          <Box as='ul' listStyleType='none'>
            <NextLink href='/stories' passHref>
              <Link
                _hover={{ textDecor: 'none' }}
                as='li'
                py={1}
                fontSize={{ base: 'xs', md: 'sm' }}
              >
                Latest Stories
              </Link>
            </NextLink>
            <NextLink href='/terms' passHref>
              <Link
                _hover={{ textDecor: 'none' }}
                as='li'
                py={1}
                fontSize={{ base: 'xs', md: 'sm' }}
              >
                Terms
              </Link>
            </NextLink>
            <NextLink href='/privacy' passHref>
              <Link
                _hover={{ textDecor: 'none' }}
                as='li'
                py={1}
                fontSize={{ base: 'xs', md: 'sm' }}
              >
                Privacy
              </Link>
            </NextLink>
          </Box>
        </Box>
        <Box display={{ base: 'none', md: 'block' }}>
          <Heading
            as='h5'
            fontSize={{ base: 'sm', md: 'md' }}
            mb={4}
            textTransform='uppercase'
          >
            Newsletter sign-up
          </Heading>

          <Flex borderWidth={1} borderColor='black'>
            <Input
              type='email'
              placeholder='email@example.com'
              borderWidth={0}
              _focus={{ outline: 'none' }}
              rounded={0}
            />
            <Button
              bg='#ffc600'
              rounded={0}
              _hover={{ bg: '#ffc600' }}
              _active={{ bg: '#ffc600' }}
            >
              Go!
            </Button>
          </Flex>
        </Box>
      </Grid>

      <Box display={{ base: 'block', md: 'none' }} py={10}>
        <Heading
          as='h5'
          fontSize={{ base: 'sm', md: 'md' }}
          mb={4}
          textTransform='uppercase'
        >
          Newsletter sign-up
        </Heading>

        <Flex borderWidth={1} borderColor='black'>
          <Input
            type='email'
            placeholder='email@example.com'
            borderWidth={0}
            _focus={{ outline: 'none' }}
            rounded={0}
          />
          <Button
            bg='#ffc600'
            rounded={0}
            _hover={{ bg: '#ffc600' }}
            _active={{ bg: '#ffc600' }}
          >
            Go!
          </Button>
        </Flex>
      </Box>
    </Container>
  );
};

export default Footer;
