import React from 'react';
import { imageBuilder } from 'lib/sanity';
import moment from 'moment';

const BlogCard = ({ post }) => {
  const image = imageBuilder.image(post?.coverImage).url();

  return (
    <div>
      <img className='h-48 w-full object-cover' src={image} alt={post?.title} />
    </div>
  );
};

export default BlogCard;
