import React from 'react';
import {
  Modal,
  ModalOverlay,
  ModalContent,
  ModalBody,
  ModalCloseButton,
  Box,
  Grid,
  Heading,
  Text,
  Button,
  Flex,
} from '@chakra-ui/react';
import { Formik } from 'formik';
import FormInput from '../Form/FormInput';
import FormTextArea from '../Form/FormTextArea';
import FormSelect from '../Form/FormSelect';
import { GoogleMap, useLoadScript, Marker } from '@react-google-maps/api';

const containerStyle = {
  width: '100%',
  height: '400px',
};

const center = {
  lat: 9.076703,
  lng: 7.386115,
};

const libraries = ['places'];

const AddVoiceModal = ({ isOpen, onClose }) => {
  const mapRef = React.useRef();

  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: process.env.NEXT_PUBLIC_API_KEY,
    libraries,
  });

  const onMapLoad = React.useCallback((map) => {
    mapRef.current = map;
  }, []);

  if (loadError) return 'Error';
  if (!isLoaded) return 'Loading...';

  return (
    <Modal isOpen={isOpen} onClose={onClose} isCentered size='3xl'>
      <ModalOverlay />
      <ModalContent>
        <ModalCloseButton zIndex={100} />
        <ModalBody
          p={{ base: 4, md: 10 }}
          maxH={{ base: 125, md: 127 }}
          overflowY='scroll'
        >
          <Box textAlign='center'>
            <Heading
              as='h3'
              fontSize={{ base: '2xl', md: '3xl' }}
              color='brand.300'
            >
              Add Your Voice
            </Heading>
            <Text fontSize='sm' color='gray.600'>
              Report case of dead or missing person
            </Text>
          </Box>
          <Formik
            initialValues={{
              fullName: '',
              age: '',
              date: '',
              occurred_at: '',
              description: '',
              nationality: 'Nigerian',
              status: '',
              external_reference: '',
              reference_title: '',
              location: {},
            }}
          >
            {({
              values,
              handleBlur,
              handleChange,
              handleSubmit,
              isSubmitting,
              setFieldValue,
            }) => (
              <Box as='form' onSubmit={handleSubmit}>
                <Grid
                  templateColumns={{ md: 'repeat(2, 1fr)' }}
                  gap={{ base: 4, md: 6 }}
                  mt={{ base: 6, md: 10 }}
                >
                  <FormInput
                    isRequired
                    placeholder='Provide the full name of the person'
                    label='Full Name'
                    name='fullName'
                    value={values.fullName}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <FormInput
                    isRequired
                    placeholder='Provide Age(In Years)'
                    label='Age'
                    name='age'
                    value={values.age}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <FormInput
                    isRequired
                    type='date'
                    label='Date Of Occurrence'
                    placeholder='Tell us when this happened, to the best of your knowledge'
                    name='date'
                    value={values.date}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <FormInput
                    isRequired
                    label='Occurred at'
                    placeholder='Place of occurance'
                    name='occurred_at'
                    value={values.occurred_at}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <FormInput
                    isRequired
                    label='Nationality'
                    placeholder='Where are you from?'
                    name='nationality'
                    value={values.nationality}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <FormSelect
                    label='What is their status?'
                    placeholder='Select an option'
                    name='status'
                    value={values.status}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <FormInput
                    label='External Reference'
                    placeholder='A link to a media resource that provides further clarity'
                    type='url'
                    name='external_reference'
                    value={values.external_reference}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                  <FormInput
                    label='Reference Title'
                    placeholder='Give your external reference a short title'
                    name='reference_title'
                    value={values.reference_title}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </Grid>
                <Box mt={{ base: 4, md: 6 }}>
                  <GoogleMap
                    mapContainerStyle={containerStyle}
                    center={center}
                    zoom={10}
                    onClick={(e) => {
                      setFieldValue('location', {
                        lat: e.latLng.lat(),
                        lng: e.latLng.lng(),
                        time: new Date(),
                      });
                    }}
                    onLoad={onMapLoad}
                  >
                    <Marker
                      position={{
                        lat: values?.location?.lat,
                        lng: values?.location?.lng,
                      }}
                    />
                  </GoogleMap>
                </Box>
                <Box mt={{ base: 4, md: 6 }}>
                  <FormTextArea
                    label='Description'
                    placeholder='Keep it under 255 characters.'
                    max='255'
                    min='80'
                    name='description'
                    value={values.description}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </Box>
                <Flex align='center' justify='center' my={{ base: 5, md: 6 }}>
                  <Button
                    bg='brand.100'
                    color='white'
                    px={{ base: 40, md: 32 }}
                    py={{ md: 6 }}
                    _hover={{ bg: 'brand.100' }}
                    _action={{ bg: 'brand.100' }}
                    rounded='30px'
                    type='submit'
                    isLoading={isSubmitting}
                  >
                    Submit
                  </Button>
                </Flex>
              </Box>
            )}
          </Formik>
        </ModalBody>
      </ModalContent>
    </Modal>
  );
};

export default AddVoiceModal;
