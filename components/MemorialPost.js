import React from 'react';
import { imageBuilder } from 'lib/sanity';
import { Box, Button, Collapse, Heading, Image } from '@chakra-ui/react';
import BlockContent from '@sanity/block-content-to-react';

const MemorialPost = ({ post }) => {
  const [show, setShow] = React.useState(false);

  const handleToggle = () => setShow(!show);

  const image = imageBuilder.image(post?.coverImage).url();
  return (
    <Box
      h={{ base: 108, md: 120, lg: 123 }}
      w='100%'
      mb={{ base: 108, md: 120, lg: 130 }}
      _last={{ mb: { base: 20, md: 32, lg: 40 } }}
    >
      <Image src={image} h='100%' w='100%' objectFit='cover' />
      <Box mt={2}>
        <Heading
          as='h3'
          fontSize={{ base: '3xl', md: '4xl', lg: '5xl' }}
          color='brand.100'
          fontStyle='italic'
        >
          {post?.name},
        </Heading>
        <Heading
          as='h3'
          fontSize={{ base: '3xl', md: '4xl', lg: '5xl' }}
          color='brand.100'
          fontStyle='italic'
          mt={-4}
        >
          {post?.age}
        </Heading>
      </Box>

      <Box mt={6}>
        <Heading as='h2' fontSize={{ base: '4xl', md: '5xl', lg: '6xl' }}>
          {post?.title}
        </Heading>
        <Box
          mt={10}
          w={{ md: 110 }}
          lineHeight='1.65em'
          fontFamily='text'
          fontWeight={500}
          fontSize={{ md: '17px' }}
          css={{ wordSpacing: '0px' }}
          letterSpacing='normal'
          color='#333'
        >
          <Collapse startingHeight={140} in={show}>
            <BlockContent
              blocks={post?.body}
              projectId={process.env.NEXT_PUBLIC_SANITY_PROJECT_ID}
              dataset='production'
            />
          </Collapse>
          <Button
            bg='none'
            _hover={{ bg: 'none' }}
            _active={{ bg: 'none' }}
            size='sm'
            onClick={handleToggle}
            mt='1rem'
          >
            Read {show ? 'Less' : 'More'}
          </Button>
        </Box>
      </Box>
    </Box>
  );
};

export default MemorialPost;
