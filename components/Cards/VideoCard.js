import React from 'react';
import { Box, Flex, Icon, Image, Text } from '@chakra-ui/react';
import { BiPlay } from 'react-icons/bi';

const VideoCard = () => {
  return (
    <Box minW={56} mr={6}>
      <Box h={{ md: 64 }} pos='relative'>
        <Image h='100%' w='100%' objectFit='cover' src='/banner-5.png' />
        <Box pos='absolute' left='40%' top='50%' translate='-50% 50%'>
          <Flex
            align='center'
            justify='center'
            w={8}
            h={8}
            rounded='100%'
            borderWidth={1}
            borderColor='white'
          >
            <Icon as={BiPlay} boxSize={4} />
          </Flex>
        </Box>
      </Box>
      <Box mt={4}>
        <Text fontSize='sm' color='gray.300'>
          Category
        </Text>
        <Text fontFamily='heading'>
          Lorem ipsum dolor sit amet consectetur adipisicing elit.
        </Text>
      </Box>
    </Box>
  );
};

export default VideoCard;
