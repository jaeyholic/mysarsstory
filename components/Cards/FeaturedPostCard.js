import React from 'react';
import { Box, Heading, Image, Link, Text } from '@chakra-ui/react';
import NextLink from 'next/link';
import { imageBuilder } from 'lib/sanity';
import moment from 'moment';

const FeaturedPostCard = ({ post }) => {
  const image = imageBuilder.image(post?.coverImage).url();
  return (
    <NextLink href={`/stories/${post?.slug}`} passHref>
      <Link _hover={{ textDecor: 'none' }}>
        <Box bg='white' w={80} minH={{ md: 122.5 }}>
          <Box h={{ base: 90, md: 108 }}>
            <Image h='100%' w='100%' objectFit='cover' src={image} />
          </Box>
          <Box p={4} fontSize='sm' fontWeight={700} pos='relative'>
            <Text textTransform='uppercase' color='gray.600'>
              {post?.category}
            </Text>

            <Box mt={{ md: 4 }}>
              <Heading
                as='h4'
                fontSize={{ base: 'xl', md: '2xl' }}
                _hover={{
                  textDecorationLine: 'underline',
                  textDecorationColor: '#dd0d0d',
                  textDecorationThickness: 1,
                }}
                transition='all 150ms ease-in-out'
              >
                {post?.title}
              </Heading>
            </Box>

            <Box
              pos={{ md: 'absolute' }}
              bottom={{ md: -10 }}
              mt={{ base: 4, md: 0 }}
            >
              <Text>{moment(post?.date).fromNow()}</Text>
            </Box>
          </Box>
        </Box>
      </Link>
    </NextLink>
  );
};

export default FeaturedPostCard;
