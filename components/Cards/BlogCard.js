import React from 'react';
import { Box, Flex, Heading, Image, Link, Text } from '@chakra-ui/react';
import NextLink from 'next/link';
import { imageBuilder } from 'lib/sanity';
import moment from 'moment';

const BlogCard = ({ post }) => {
  const image = imageBuilder.image(post?.coverImage).url();

  return (
    <NextLink href={`/stories/${post?.slug}`} passHref>
      <Link _hover={{ textDecor: 'none' }}>
        <Flex bg='white' h={{ base: 32, md: 40 }}>
          <Box w='40%'>
            <Image h='100%' w='100%' objectFit='cover' src={image} />
          </Box>
          <Box w='60%' p={4} fontSize='sm' fontWeight={700} pos='relative'>
            <Text textTransform='uppercase' color='gray.600'>
              {post?.category}
            </Text>
            <Heading
              _hover={{
                textDecorationLine: 'underline',
                textDecorationColor: '#dd0d0d',
                textDecorationThickness: 1,
              }}
              transition='all 150ms ease-in-out'
              as='h5'
              fontSize={{ md: 'xl' }}
              my={{ md: 2 }}
            >
              {post?.title}
            </Heading>
            <Text
              pos='absolute'
              bottom={{ base: 2, md: 1 }}
              mt={{ base: 3, md: 0 }}
            >
              {moment(post?.date).format('LL')}
            </Text>
          </Box>
        </Flex>
      </Link>
    </NextLink>
  );
};

export default BlogCard;
