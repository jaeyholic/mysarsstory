import React from 'react';
import NextLink from 'next/link';
import { Box, Heading, Image, Link, Text } from '@chakra-ui/react';
import moment from 'moment';
import { imageBuilder } from 'lib/sanity';

const FeaturedHeroPost = ({ post }) => {
  const image = imageBuilder.image(post?.coverImage).url();

  return (
    <NextLink href={`/stories/${post?.slug}`} passHref>
      <Link _hover={{ textDecor: 'none', bg: 'rgba(255,255,255,.5)' }}>
        <Box
          w='100vw'
          h={{ base: '60vh', xl: '70vh' }}
          bg='black'
          pos='absolute'
          top={0}
          overflow='hidden'
        >
          <Box pos='relative'>
            <Image
              w='100%'
              h={{ base: '60vh', xl: '100%' }}
              objectFit='cover'
              src={image}
            />
            <Box
              pos='absolute'
              top={0}
              left={0}
              right={0}
              bottom={0}
              bg='rgba(0,0,0,.4)'
            ></Box>
          </Box>

          <Box
            pos='absolute'
            zIndex={10}
            bottom={20}
            left={{ xl: 72 }}
            px={{ base: 4, md: 10, lg: 16, xl: 0 }}
            color='white'
            fontFamily='body'
            fontSize={{ md: 'lg' }}
          >
            <Box>
              <Text fontWeight='700' fontSize='sm'>
                {moment().format('LL')}
              </Text>
              <Heading as='h2' fontSize={{ base: '3xl', md: '5xl', lg: '6xl' }}>
                Today on My Sars Story
              </Heading>
            </Box>
            <Box mt={4}>
              <Text
                fontWeight='700'
                textTransform='uppercase'
                fontSize={{ base: 'sm', lg: 'md' }}
              >
                {post?.category}
              </Text>
              <Heading
                as='h3'
                fontSize={{ base: 'xl', md: '2xl', md: '3xl' }}
                _hover={{
                  textDecorationLine: 'underline',
                  textDecorationColor: '#dd0d0d',
                  textDecorationThickness: 2,
                }}
                transition='all 150ms ease-in-out'
              >
                {post?.title}
              </Heading>
            </Box>
          </Box>
        </Box>
      </Link>
    </NextLink>
  );
};

export default FeaturedHeroPost;
