import { Box, Flex, Icon, Image, Link, Text } from '@chakra-ui/react';
import React from 'react';
import NextLink from 'next/link';
import { BiSearch } from 'react-icons/bi';
import { useRouter } from 'next/router';
import { Menu } from '@headlessui/react';
import { AnimatePresence, motion } from 'framer-motion';
import { NextSeo } from 'next-seo';

const MotionBox = motion.custom(Box);

const Navbar = ({ onOpen }) => {
  const router = useRouter();
  const { pathname } = router;

  const menuAnimation = {
    show: {
      opacity: 1,
      y: 0,
      transition: { duration: 0.3 },
    },
    hidden: {
      opacity: 0,
      y: -20,
      transition: { duration: 0.3 },
    },
  };

  const menus = [
    { id: 1, name: 'Memorials', path: '/memorials' },
    { id: 2, name: 'Stories', path: '/stories' },
    { id: 3, name: 'Watch', path: '/watch' },
    // { id: 4, name: 'Read', path: '/read' },
    { id: 5, name: 'Listen', path: '/listen' },
    // { id: 6, name: 'Resources', path: '/resources' },
  ];

  return (
    <Flex
      w='100vw'
      h={20}
      px={{ base: 4, md: 10, lg: 16, xl: 72 }}
      align='center'
      justify='space-between'
      pos='relative'
      zIndex={60}
      color='white'
    >
      <NextLink href='/' passHref>
        <Link _hover={{ textDecor: 'none' }} _active={{ outline: 'none' }}>
          <Box>
            <Image h={12} src='/mss_logo.png' alt='MSS Logo' />
          </Box>
        </Link>
      </NextLink>

      <Box>
        <Menu>
          {({ open }) => (
            <>
              <Menu.Button
                as={Box}
                display={{ base: 'block', md: 'none' }}
                fontWeight='bold'
                fontSize='lg'
                cursor='pointer'
              >
                {open ? 'Close' : 'Menu'}
              </Menu.Button>
              <AnimatePresence>
                {open && (
                  <Menu.Items
                    as={MotionBox}
                    variants={menuAnimation}
                    initial='hidden'
                    animate='show'
                    exit='hidden'
                    static
                    pos='absolute'
                    right={0}
                    zIndex={10}
                    shadow='sm'
                    w='100%'
                    mt={4}
                    rounded='sm'
                    _focus={{ outline: 'none' }}
                    fontWeight='bold'
                    bg='white'
                    color='#333'
                  >
                    <AnimatePresence>
                      {menus.map(({ id, name, path }) => (
                        <Menu.Item
                          key={id}
                          as={MotionBox}
                          w='100%'
                          variants={{
                            hidden: (id) => ({
                              opacity: 0,
                              y: -50 * id,
                            }),
                            show: (id) => ({
                              opacity: 1,
                              y: 0,
                              transition: {
                                delay: id * 0.1,
                              },
                            }),
                          }}
                          initial='hidden'
                          animate='show'
                          exit='hidden'
                          custom={id}
                        >
                          {({ active }) => (
                            <Box
                              py={4}
                              whiteSpace='nowrap'
                              px={8}
                              textAlign='center'
                              _hover={{ bg: 'gray.100' }}
                            >
                              <NextLink href={path} passHref>
                                <Link d='block' color={active && 'brand.100'}>
                                  {name}
                                </Link>
                              </NextLink>
                            </Box>
                          )}
                        </Menu.Item>
                      ))}
                    </AnimatePresence>
                    <Menu.Item>
                      <Box
                        py={4}
                        d='block'
                        whiteSpace='nowrap'
                        px={8}
                        bg='brand.100'
                        color='white'
                        textAlign='center'
                        onClick={onOpen}
                        mt={4}
                        cursor='pointer'
                      >
                        Add your voice
                      </Box>
                    </Menu.Item>
                  </Menu.Items>
                )}
              </AnimatePresence>
            </>
          )}
        </Menu>
      </Box>

      <Flex
        as='ul'
        textTransform='uppercase'
        fontWeight={700}
        display={{ base: 'none', md: 'flex' }}
      >
        <NextLink href='/stories' passHref>
          <Link _hover={{ textDecor: 'none' }} px={{ md: 4, lg: 8 }}>
            {pathname === '/stories' && (
              <Box
                pos='absolute'
                top={0}
                borderTopWidth={4}
                borderTopColor='white'
                rounded='md'
                w={20}
                ml={-2}
              ></Box>
            )}
            Stories
          </Link>
        </NextLink>
        <NextLink href='/memorials' passHref>
          <Link _hover={{ textDecor: 'none' }} px={{ md: 4, lg: 8 }}>
            {pathname === '/memorials' && (
              <Box
                pos='absolute'
                top={0}
                borderTopWidth={4}
                borderTopColor='white'
                rounded='md'
                w={28}
                ml={-3}
              ></Box>
            )}
            Memorials
          </Link>
        </NextLink>
        {/* <NextLink href='/read' passHref>
          <Link _hover={{ textDecor: 'none' }} px={{ md: 4, lg: 8 }}>
            {pathname === '/read' && (
              <Box
                pos='absolute'
                top={0}
                borderTopWidth={4}
                borderTopColor='brand.100'
                rounded='md'
                w={20}
                ml={-3}
              ></Box>
            )}
            Read
          </Link>
        </NextLink> */}
        <NextLink href='/watch' passHref>
          <Link _hover={{ textDecor: 'none' }} px={{ md: 4, lg: 8 }}>
            {pathname === '/watch' && (
              <Box
                pos='absolute'
                top={0}
                borderTopWidth={4}
                borderTopColor='brand.100'
                rounded='md'
                w={20}
                ml={-3}
              ></Box>
            )}
            Watch
          </Link>
        </NextLink>
        <NextLink href='/listen' passHref>
          <Link _hover={{ textDecor: 'none' }} px={{ md: 4, lg: 8 }}>
            {pathname === '/listen' && (
              <Box
                pos='absolute'
                top={0}
                borderTopWidth={4}
                borderTopColor='brand.100'
                rounded='md'
                w={20}
                ml={-3}
              ></Box>
            )}
            Listen
          </Link>
        </NextLink>
        <Box
          pl={{ md: 8 }}
          as='button'
          rolee='button'
          aria-label='search button'
        >
          <Icon as={BiSearch} boxSize={5} strokeWidth={1} />
        </Box>
      </Flex>
    </Flex>
  );
};

export default Navbar;
