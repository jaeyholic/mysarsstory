import { FormControl, FormLabel, Select } from '@chakra-ui/react';
import React from 'react';

const FormSelect = ({ label, isRequired, ...props }) => {
  return (
    <FormControl isRequired={isRequired}>
      <FormLabel>{label}</FormLabel>
      <Select variant='filled' {...props} rounded='0px' h={12}>
        <option value='brutality'>Police Brutality</option>
        <option value='dead'>Dead</option>
        <option value='missing'>Missing</option>
      </Select>
    </FormControl>
  );
};

export default FormSelect;
