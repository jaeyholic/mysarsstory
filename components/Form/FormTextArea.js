import { FormControl, FormLabel, Textarea } from '@chakra-ui/react';
import React from 'react';

const FormTextArea = ({ isRequired, label, ...props }) => {
  return (
    <FormControl isRequired={isRequired}>
      <FormLabel>{label}</FormLabel>
      <Textarea {...props} rounded='0px' variant='filled' resize='none' />
    </FormControl>
  );
};

export default FormTextArea;
