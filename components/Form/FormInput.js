import { FormControl, FormLabel, Input } from '@chakra-ui/react';
import React from 'react';

const FormInput = ({ label, isRequired, ...props }) => {
  return (
    <FormControl isRequired={isRequired}>
      <FormLabel>{label}</FormLabel>
      <Input {...props} rounded='0px' h={12} variant='filled' />
    </FormControl>
  );
};

export default FormInput;
