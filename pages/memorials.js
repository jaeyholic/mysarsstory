import { Box, Container, Grid, Heading } from '@chakra-ui/react';
import React from 'react';
import { getAllPostsForMemories } from 'lib/api';
import MemorialPost from '@components/MemorialPost';
import { NextSeo } from 'next-seo';

const memorials = ({ allPosts }) => {
  return (
    <Box>
      <NextSeo
        title='Memorials | My Sars Story'
        description='Remembering the men, women, and children killed by SARS over the years.'
      />
      <Box
        w='100vw'
        h='50vh'
        bg='black'
        pos='absolute'
        top={0}
        bottom={0}
        overflow='hidden'
        bg='brand.100'
        color='white'
      >
        <Box
          pos='absolute'
          bottom={{ base: 12, md: 20 }}
          px={{ base: 4, md: 10, lg: 16, xl: 72 }}
        >
          <Heading as='h3' fontSize={{ base: '3xl', md: '5xl', lg: '6xl' }}>
            Remembering the men, <br />
            women, and children killed <br />
            by SARS over the years.
          </Heading>
        </Box>
      </Box>

      <Container maxW={{ md: '6xl' }} my={{ base: 108, md: 117, lg: 122.5 }}>
        <Grid w={{ md: 122 }}>
          {allPosts?.map((post) => (
            <MemorialPost key={post?._id} post={post} />
          ))}
        </Grid>
      </Container>
    </Box>
  );
};

export async function getStaticProps({ preview = false }) {
  const allPosts = await getAllPostsForMemories(preview);
  return {
    props: { allPosts, preview },
  };
}

export default memorials;
