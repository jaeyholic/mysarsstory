import React from 'react';
import { getAllPostsWithSlug, getPostAndMorePosts } from 'lib/api';
import BlockContent from '@sanity/block-content-to-react';
import { Box, Container, Grid, Heading, Image, Text } from '@chakra-ui/react';
import { imageBuilder } from 'lib/sanity';
import moment from 'moment';
import BlogCard from '@components/Cards/BlogCard';
import { NextSeo } from 'next-seo';

const BlogPost = ({ post, morePosts, preview }) => {
  const image = imageBuilder.image(post?.coverImage).url();

  return (
    <Box>
      <NextSeo
        title={`${post?.title} | My Sars Story`}
        description={post?.body[0]?.children[0].text}
      />
      <Box
        w='100vw'
        h={{ base: '60vh', xl: '70vh' }}
        bg='black'
        pos='absolute'
        top={0}
        overflow='hidden'
      >
        <Box pos='relative'>
          <Image
            w='100vw'
            h={{ base: '60vh', xl: '100%' }}
            objectFit='cover'
            src={image}
          />
          <Box
            pos='absolute'
            top={0}
            left={0}
            right={0}
            bottom={0}
            bg='rgba(0,0,0,.4)'
          ></Box>
        </Box>

        <Box
          pos='absolute'
          zIndex={10}
          bottom={{ base: 12, md: 20 }}
          left={{ xl: 72 }}
          right={{ xl: 72 }}
          px={{ base: 4, md: 10, lg: 16, xl: 0 }}
          color='white'
          fontFamily='body'
          fontSize={{ md: 'lg' }}
        >
          <Box mt={4}>
            <Text
              fontWeight='700'
              textTransform='uppercase'
              fontSize={{ base: 'sm', md: 'md' }}
            >
              {post?.category}
            </Text>
            <Text my={2}>{moment(post?.date).format('LL')}</Text>
            <Heading as='h2' fontSize={{ base: '4xl', md: '5xl', lg: '6xl' }}>
              {post?.title}
            </Heading>
          </Box>
        </Box>
      </Box>

      <Container
        maxW={{ md: '3xl' }}
        mt={{ base: 110, md: 122, lg: 130, xl: 125 }}
        lineHeight='1.65em'
        fontFamily='text'
        fontWeight={500}
        fontSize={{ md: '17px' }}
        css={{ wordSpacing: '0px' }}
        letterSpacing='normal'
        color='#333'
      >
        <Box
          sx={{
            '.blog figure img': {
              marginTop: '30px',
              marginBottom: '30px',
            },
            '.blog p strong': {
              marginTop: '30px',
              display: 'block',
              fontSize: '20px',
            },
          }}
        >
          <BlockContent
            className='blog'
            blocks={post?.body}
            projectId={process.env.NEXT_PUBLIC_SANITY_PROJECT_ID}
            dataset='production'
          />
        </Box>

        <Box mt={20} mb={32}>
          <Box>
            <Heading as='h5' fontSize={{ base: 'lg', md: 'xl' }}>
              Related stories
            </Heading>
          </Box>

          <Grid templateColumns={{ md: 'repeat(2, 1fr)' }} gap={6} mt={10}>
            {morePosts?.map((post) => (
              <BlogCard key={post?._id} post={post} />
            ))}
          </Grid>
        </Box>
      </Container>
    </Box>
  );
};

export async function getStaticProps({ params, preview = false }) {
  const data = await getPostAndMorePosts(params.slug, preview);
  return {
    props: {
      preview,
      post: data?.post || null,
      morePosts: data?.morePosts || null,
    },
  };
}

export async function getStaticPaths() {
  const allPosts = await getAllPostsWithSlug();
  return {
    paths:
      allPosts?.map((post) => ({
        params: {
          slug: post.slug,
        },
      })) || [],
    fallback: true,
  };
}

export default BlogPost;
