import { getAllPostsForHome } from 'lib/api';
import React from 'react';
import {
  Box,
  Button,
  Container,
  Flex,
  Grid,
  GridItem,
  Heading,
  Text,
} from '@chakra-ui/react';
import FeaturedPostCard from '@components/Cards/FeaturedPostCard';
import FeaturedHeroPost from '@components/FeaturedHeroPost';
import BlogCard from '@components/Cards/BlogCard';
import VideoCard from '@components/Cards/VideoCard';
import { BiChat } from 'react-icons/bi';
import { NextSeo } from 'next-seo';

export default function Home({ allPosts, onOpen }) {
  const [selectedPost, setSelectedPost] = React.useState(allPosts[0]);

  React.useEffect(() => {
    let interval;
    interval = setInterval(() => {
      setSelectedPost(allPosts[Math.floor(Math.random() * allPosts.length)]);
    }, 1000 * 60 * 60 * 24);
    return () => clearInterval(interval);
  }, [selectedPost]);

  const featuredPosts = allPosts?.filter((el) =>
    el.category.includes('featured')
  );

  return (
    <Box>
      <NextSeo
        title='Home | My Sars Story'
        description="Let's hear your story"
      />

      <FeaturedHeroPost post={selectedPost} />

      <Grid
        templateColumns={{ md: 'repeat(7, 1fr)' }}
        mt={{ base: 108, md: 122.5, lg: 122.5 }}
      >
        <GridItem
          colSpan={{ md: 3 }}
          bg='tomato'
          h={{ base: 28, md: 36, lg: 48 }}
        />
        <GridItem
          colSpan={{ md: 2 }}
          bg='papayawhip'
          h={{ base: 28, md: 36, lg: 48 }}
        />
        <GridItem
          colSpan={{ md: 2 }}
          bg='paleturquoise'
          h={{ base: 28, md: 36, lg: 48 }}
        />
      </Grid>

      <Container
        maxW={{ lg: '5xl', xl: '6xl' }}
        mt={{ base: 16, md: 28 }}
        pb={20}
      >
        <Box mb={10}>
          <Heading as='h3' fontSize={{ base: '2xl', md: '4xl' }}>
            Trending stories
          </Heading>
        </Box>

        <Grid
          d={{ base: 'flex', lg: 'grid' }}
          overflowX={{ base: 'scroll', lg: 'auto' }}
          templateColumns={{ lg: 'repeat(3, 1fr)' }}
          gap={{ base: 10, lg: 12 }}
        >
          {featuredPosts?.map((item) => (
            <FeaturedPostCard key={item?._id} post={item} />
          ))}
        </Grid>
      </Container>

      <Box bg='black' py={16} color='white'>
        <Container maxW={{ lg: '5xl', xl: '6xl' }}>
          <Heading as='h4'>Watch</Heading>

          <Flex
            align='center'
            mt={{ base: 12, md: 16 }}
            overflowX={{ base: 'scroll' }}
          >
            <VideoCard />
            <VideoCard />
            <VideoCard />
            <VideoCard />
          </Flex>
        </Container>
      </Box>

      <Container maxW={{ md: '6xl' }} mt={{ base: 20, md: 28 }} pb={20}>
        <Box mb={10}>
          <Heading>More Stories</Heading>
        </Box>

        <Grid
          templateColumns={{ md: 'repeat(2, 1fr)', lg: 'repeat(3, 1fr)' }}
          gap={{ base: 10, md: 12 }}
          mt={{ md: 10 }}
        >
          {allPosts?.slice(0, 6)?.map((post) => (
            <BlogCard key={post?._id} post={post} />
          ))}
        </Grid>
      </Container>

      <Box bg='brand.100' color='white' py={{ base: 28, md: 32 }} my={20}>
        <Container maxW={{ md: '6xl' }} textAlign='center'>
          <Heading as='h3' fontSize={{ base: '4xl', md: '5xl' }}>
            Have you been a victim?
          </Heading>
          <Text
            fontSize={{ md: 'lg' }}
            fontWeight={700}
            lineHeight='1.5'
            mt={4}
          >
            The Special Anti-Robbery Squad (SARS), a unit of the Nigerian Police
            has a long record of human rights abuses and violations. Many lives
            have been lost, dreams shattered and many others have experienced
            incalculable losses as a result of their activities. As part of the
            #ENDSARS movement, we're documenting the toll of SARS across Nigeria
            - putting names and faces to the numbers.
          </Text>
          <Box
            mt={6}
            borderWidth={1}
            borderColor='white'
            rounded='md'
            w={{ base: 48, md: 56 }}
            mx='auto'
          >
            <Button
              bg='none'
              _hover={{ bg: 'none' }}
              _active={{ bg: 'none' }}
              fontWeight={700}
              fontSize={{ md: 'lg' }}
              leftIcon={<BiChat />}
              color='white'
              onClick={onOpen}
            >
              Add your voice
            </Button>
          </Box>
        </Container>
      </Box>
    </Box>
  );
}

export async function getStaticProps({ preview = false }) {
  const allPosts = await getAllPostsForHome(preview);
  return {
    props: { allPosts, preview },
  };
}
