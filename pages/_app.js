import 'styles/globals.css';
import { Box, ChakraProvider, useDisclosure } from '@chakra-ui/react';
import { theme } from 'theme/theme';
import 'styles/nprogress.css';
import React from 'react';
import nProgress from 'nprogress';
import Router, { useRouter } from 'next/router';
import Navbar from '@components/Navbar';
import Footer from '@components/Footer';
import AddYourVoice from '@components/Modal/AddYourVoice';
import * as gtag from 'lib/gtag';

Router.events.on('routeChangeStart', () => nProgress.start());
Router.events.on('routeChangeComplete', () => nProgress.done());
Router.events.on('routeChangeError', () => nProgress.done());

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  const { isOpen, onOpen, onClose } = useDisclosure();

  React.useEffect(() => {
    const handleRouteChange = (url) => {
      gtag.pageview(url);
    };
    router.events.on('routeChangeComplete', handleRouteChange);
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange);
    };
  }, [router.events]);

  return (
    <ChakraProvider theme={theme}>
      <Box pos='relative' bg='gray.100' minH='100vh' overflow='hidden'>
        <AddYourVoice isOpen={isOpen} onClose={onClose} />
        <Navbar onOpen={onOpen} />
        <Component {...pageProps} onOpen={onOpen} />
        <Footer />
      </Box>
    </ChakraProvider>
  );
}

export default MyApp;
