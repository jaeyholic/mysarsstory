import { Box, Container, Grid, Heading, Image } from '@chakra-ui/react';
import BlogCard from '@components/Cards/BlogCard';
import FeaturedPostCard from '@components/Cards/FeaturedPostCard';
import { NextSeo } from 'next-seo';
import React from 'react';
import { getAllPostsForHome } from 'lib/api';

const story = ({ allPosts }) => {
  const featuredPosts = allPosts?.filter((el) =>
    el.category.includes('featured')
  );

  return (
    <Box>
      <NextSeo
        title='Stories | My Sars Story'
        description='Remembering the men, women, and children killed by SARS over the years.'
      />
      <Box
        w='100vw'
        h='50vh'
        bg='black'
        pos='absolute'
        top={0}
        bottom={0}
        overflow='hidden'
        color='white'
      >
        <Box
          pos='absolute'
          top={0}
          bottom={0}
          right={0}
          left={0}
          w='100vw'
          h='100%'
        >
          <Image
            h='100%'
            w='100%'
            objectFit='cover'
            src='/SARS-Img.png'
            alt='Stories banner'
          />
        </Box>
        <Box
          pos='absolute'
          bottom={20}
          px={{ base: 4, md: 10, md: 16, xl: 72 }}
        >
          <Heading as='h3' fontSize={{ base: '4xl', md: '6xl' }}>
            Stories
          </Heading>
        </Box>
      </Box>

      <Box mt={{ base: 85, md: 108, lg: 95 }}>
        <Box
          borderBottomWidth={10}
          w='100%'
          borderBottomColor='green.600'
        ></Box>
        <Box borderBottomWidth={10} w='100%' borderBottomColor='#fff'></Box>
        <Box
          borderBottomWidth={10}
          w='100%'
          borderBottomColor='green.600'
        ></Box>
      </Box>

      <Container
        maxW={{ lg: '5xl', xl: '6xl' }}
        mt={{ base: 20, md: 32, lg: 48 }}
        mb={32}
        pb={{ md: 20 }}
      >
        <Box mb={10}>
          <Heading as='h3' fontSize={{ base: '2xl', md: '4xl' }}>
            Trending posts
          </Heading>
        </Box>

        <Grid
          d={{ base: 'flex', lg: 'grid' }}
          overflowX={{ base: 'scroll', lg: 'auto' }}
          templateColumns={{ lg: 'repeat(3, 1fr)' }}
          gap={{ base: 10, lg: 12 }}
        >
          {featuredPosts?.map((item) => (
            <FeaturedPostCard key={item?._id} post={item} />
          ))}
        </Grid>

        <Grid
          templateColumns={{ md: 'repeat(2, 1fr)', lg: 'repeat(3, 1fr)' }}
          gap={{ base: 10, md: 12 }}
          mt={{ base: 10, md: 12 }}
        >
          {allPosts?.map((post) => (
            <BlogCard key={post?._id} post={post} />
          ))}
        </Grid>
      </Container>
    </Box>
  );
};

export async function getStaticProps({ preview = false }) {
  const allPosts = await getAllPostsForHome(preview);
  return {
    props: { allPosts, preview },
  };
}

export default story;
